![Logo](https://i.imgur.com/InwtXNX.png)

# Certified Tech Dev - Proyecto Integrador

Grupo # 5
Camada 1

*WEB HUS*

[Link](http://18.232.222.109:3000/)

*Documentacion de Infraestructura*

https://docs.google.com/presentation/d/1LpKQAp9SBM4EkW4O4gYoItm5sedM0vzOdHwQMx_SVnw/edit#slide=id.p

*Testing*

- test cases Hus-Front
- test cases Hus-Back
- template ciclo
- Documentacion

https://docs.google.com/spreadsheets/d/1JxLruSmA2EN3W8gtkx64NcT5zVriyq1F/edit#gid=38293198


*Archivos complementarios*

- Test-Postman
- Assets
- UML Diagram.png
- EER Diagram.png
- entityManagerFactory(EntityManagerFactoryBuilder).png
- Swagger UI documentation.pdf
- Authorizations per path.xlsx
- Sprint 3  - Planning
- Sprint 4  - Planning
- Test cases.xlsx
- hus-0.0.1-SNAPSHOT.jar
- jest_test.png
- Diagramas de Red.jpg
- DumpAAAAMMDD.sql


https://drive.google.com/drive/folders/1c_c1ZBzAPTGZOI-Gy_qr7YAz9JnPbnnD?usp=sharing




