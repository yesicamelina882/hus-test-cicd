![Logo](https://i.imgur.com/InwtXNX.png)

# HUS - FRONT

Repositorio Front-End del proyecto HUS.

# TESTING

## JEST

A continuacion presentamos una imagen con nuestro coverage.

![alt text](https://i.imgur.com/BQBhroc.png)
https://i.imgur.com/BQBhroc.png

Pueden ingresar igualmente al index en ./coverage/lcov-report/index.html que se actualiza automaticamente con cada vez que hacemos un coverage para ver en detalle.
