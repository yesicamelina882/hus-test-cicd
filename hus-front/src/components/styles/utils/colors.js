export const colors = {
  /**color1- original*/
  primary: "#FFF",
  secondary: "#545776",
  highlight: "#1DBEB4",

  formText: "#383b58",
  error: "#B00020",

  alert: "#B00020",
  back: "#DFE4EA",
};
