import React from "react";

import { DescriptionProductStyled } from "./DescriptionProductStyled";

export default function DescriptionProduct({ description, category }) {
  return (
    // EN COMPONENTS/STYLES/PRODUCT/HEADINGNAMESTYLED TENES EL WRAPPER DE STYLED COMPONENT DE ESTE COMPONENTE
    <DescriptionProductStyled>
      <div className="titleContainer">
        <h3>{category}</h3>
        <p>{description}</p>
      </div>
    </DescriptionProductStyled>
  );
}
