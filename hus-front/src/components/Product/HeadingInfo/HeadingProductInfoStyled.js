import styled from "styled-components";
import { colors } from "./../../styles/utils/colors";
import { media } from "./../../styles/utils/media";

const HeadingProductInfoStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background-color: #d3d3d3;
  color: ${colors.secondary};
  padding: 10px 30px;
  .heading_info_title {
    h4 {
      max-width: 390px;
    }
  }
  .scoreContainer {
    display: flex;
    div:nth-child(1n) {
      display: flex;
      flex-direction: column;
    }
    div:nth-child(2n) {
      display: flex;
      align-items: center;
      justify-content: center;
      width: 35px;
      height: 35px;
      background-color: ${colors.secondary};
      color: ${colors.primary};
      margin-left: 10px;
      border-radius: 10px;
    }

    /* ${media.tablet} {
        h2{
            font-size:2rem;
        }
      }
    ${media.mobile} {
        height:260px;
        h2{
            font-size:1.6rem;
        }
      } */
  }
`;
export default HeadingProductInfoStyled;
