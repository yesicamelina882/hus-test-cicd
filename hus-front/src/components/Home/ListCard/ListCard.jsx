import React from "react";
import Button from "./../../shared/Button/Button";
import StartNotRated from "./../../../images/StartNotRated";
import StartRated from "./../../../images/StartRated";
import { selectFeatureIcon } from "./../../Product/utils";
import { useState } from "react";
import { Link } from "react-router-dom";
import Like from "../../shared/Like/Like";

export default function ListCard(props) {
  const [more, setMore] = useState(false);

  const score = props.score;
  const handleStarts = (score) => {
    const arr = [];
    for (let i = 0.5; i < 5; i++) {
      if (i < score) {
        arr.push(<StartRated />);
      } else {
        arr.push(<StartNotRated />);
      }
    }
    return arr;
  };

  const [like, setLike] = useState(false);

  return (
    <li key={props.id} className="list-card">   
      <Like checked={like} setChecked={setLike} id={props.id}/>
      <img src={props.img} alt={props.alt} className="list-img" /> 
      <div className="list-card-info">
        <div className="header-card">
          <div className="header-name">
            <span className="categorie">{props.categorie}</span>
            {handleStarts(score)}
            <h3 className="name">{props.name}</h3>
          </div>
          <div className="score">
            <div>{props.score}</div>
            <p>
              {props.score > 4.5
                ? "Excelente"
                : props.score > 3.8
                ? "Muy bueno"
                : props.score > 3
                ? "Bueno"
                : "Normal"}
            </p>
          </div>
        </div>

        <p className="location">
          <i className="fas fa-map-marker-alt"></i>
          {"A 960 m del centro - "}
          <Link to={"/product/" + props.id}>
            <a href="#" className="map">
              MOSTRAR EN EL MAPA
            </a>
          </Link>
          {props.icons.map((element) => {
            return (
              <spam className="features">
                {selectFeatureIcon(element.icon)}
              </spam>
            );
          })}
        </p>
        <p className="description">
          {props.description.slice(0, 70)}
          {more === false ? (
            <spam className="see-more" onClick={() => setMore(true)}>
              {" Ver mas... "}
            </spam>
          ) : (
            <spam className="more-description">
              {props.description.slice(70, props.description.lenght)}
            </spam>
          )}
        </p>
        <Button theme="secundary" text="Ver mas" to={"/product/" + props.id} />
      </div>
    </li>
    
  );
}
