import styled from "styled-components";
import { colors } from "./../../../components/styles/utils/colors";
import { media } from "./../../../components/styles/utils/media";

export const UserContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: end;
  color: ${colors.secondary};
  div {
    display: flex;
    .userIcono {
      width: 50px;
      height: 50px;
      border-radius: 50%;
      background-color: ${colors.secondary};
      display: flex;
      justify-content: center;
      align-items: center;
      margin: 0 10px;
      h2 {
        color: ${colors.primary};
      }
    }
    .userBienvenida {
      display: flex;
      flex-direction: column;
      justify-content: center;
      font-weight: bold;
      .userName {
        color: ${colors.highlight};
      }
    }
  }

  ${media.mobile} {
    color: ${colors.primary};
    font-size: 1.2rem;

    div {
      display: flex;
      flex-direction: column;
      align-items: end;

      .userIcono {
        background-color: ${colors.primary};
        h2 {
          color: ${colors.secondary};
        }
      }
      .userBienvenida {
        padding-top: 10px;
        .userName {
          padding-top: 5px;
          color: ${colors.secondary};
        }
      }
    }
  }
`;
