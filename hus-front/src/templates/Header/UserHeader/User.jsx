import React ,{useContext} from "react";
import { UserContainer } from "./UserStyled";
import userContext from "../../../contexts/UserContext";

// ***TODO***
// Linea 16 mejorar validacion deberia ser null o empty no undefined

export default function User({ page }) {
  const { userData} = useContext(userContext);
  const handleLogout = () => {
    localStorage.removeItem("token");
    window.location.reload();
  };
  return (
    <UserContainer>
      {page !== "phone" ? (
        <i className="fas fa-times" onClick={handleLogout}></i>
      ) : null}

      <div>
        <div className="userIcono">
          <h2>
            {userData !== undefined
              ? userData.name.charAt(0).toUpperCase() +
                userData.lastname.charAt(0).toUpperCase()
              : null}
          </h2>
        </div>
        <div className="userBienvenida">
          <p>Hola,</p>
          <p className="userName">{userData.name + " " + userData.lastname}</p>
        </div>
      </div>
    </UserContainer>
  );
}
